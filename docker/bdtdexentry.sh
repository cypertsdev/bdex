#!/bin/bash
BDTDEXD="/usr/local/bin/witness_node"

# For blockchain download
VERSION=`cat /etc/bdtdex/version`

## Supported Environmental Variables
#
#   * $BDTDEXD_SEED_NODES
#   * $BDTDEXD_RPC_ENDPOINT
#   * $BDTDEXD_PLUGINS
#   * $BDTDEXD_REPLAY
#   * $BDTDEXD_RESYNC
#   * $BDTDEXD_P2P_ENDPOINT
#   * $BDTDEXD_WITNESS_ID
#   * $BDTDEXD_PRIVATE_KEY
#   * $BDTDEXD_TRACK_ACCOUNTS
#   * $BDTDEXD_PARTIAL_OPERATIONS
#   * $BDTDEXD_MAX_OPS_PER_ACCOUNT
#   * $BDTDEXD_ES_NODE_URL
#   * $BDTDEXD_ES_START_AFTER_BLOCK
#   * $BDTDEXD_TRUSTED_NODE
#

ARGS=""
# Translate environmental variables
if [[ ! -z "$BDTDEXD_SEED_NODES" ]]; then
    for NODE in $BDTDEXD_SEED_NODES ; do
        ARGS+=" --seed-node=$NODE"
    done
fi
if [[ ! -z "$BDTDEXD_RPC_ENDPOINT" ]]; then
    ARGS+=" --rpc-endpoint=${BDTDEXD_RPC_ENDPOINT}"
fi

if [[ ! -z "$BDTDEXD_REPLAY" ]]; then
    ARGS+=" --replay-blockchain"
fi

if [[ ! -z "$BDTDEXD_RESYNC" ]]; then
    ARGS+=" --resync-blockchain"
fi

if [[ ! -z "$BDTDEXD_P2P_ENDPOINT" ]]; then
    ARGS+=" --p2p-endpoint=${BDTDEXD_P2P_ENDPOINT}"
fi

if [[ ! -z "$BDTDEXD_WITNESS_ID" ]]; then
    ARGS+=" --witness-id=$BDTDEXD_WITNESS_ID"
fi

if [[ ! -z "$BDTDEXD_PRIVATE_KEY" ]]; then
    ARGS+=" --private-key=$BDTDEXD_PRIVATE_KEY"
fi

if [[ ! -z "$BDTDEXD_TRACK_ACCOUNTS" ]]; then
    for ACCOUNT in $BDTDEXD_TRACK_ACCOUNTS ; do
        ARGS+=" --track-account=$ACCOUNT"
    done
fi

if [[ ! -z "$BDTDEXD_PARTIAL_OPERATIONS" ]]; then
    ARGS+=" --partial-operations=${BDTDEXD_PARTIAL_OPERATIONS}"
fi

if [[ ! -z "$BDTDEXD_MAX_OPS_PER_ACCOUNT" ]]; then
    ARGS+=" --max-ops-per-account=${BDTDEXD_MAX_OPS_PER_ACCOUNT}"
fi

if [[ ! -z "$BDTDEXD_ES_NODE_URL" ]]; then
    ARGS+=" --elasticsearch-node-url=${BDTDEXD_ES_NODE_URL}"
fi

if [[ ! -z "$BDTDEXD_ES_START_AFTER_BLOCK" ]]; then
    ARGS+=" --elasticsearch-start-es-after-block=${BDTDEXD_ES_START_AFTER_BLOCK}"
fi

if [[ ! -z "$BDTDEXD_TRUSTED_NODE" ]]; then
    ARGS+=" --trusted-node=${BDTDEXD_TRUSTED_NODE}"
fi

## Link the bdtdex config file into home
## This link has been created in Dockerfile, already
ln -f -s /etc/bdtdex/config.ini /var/lib/bdtdex

# Plugins need to be provided in a space-separated list, which
# makes it necessary to write it like this
if [[ ! -z "$BDTDEXD_PLUGINS" ]]; then
   exec $BDTDEXD --data-dir ${HOME} ${ARGS} ${BDTDEXD_ARGS} --plugins "${BDTDEXD_PLUGINS}"
else
   exec $BDTDEXD --data-dir ${HOME} ${ARGS} ${BDTDEXD_ARGS}
fi

#include <stdint.h>
#include <graphene/utilities/git_revision.hpp>

#define GRAPHENE_GIT_REVISION_SHA "af8a01aa9a791072f7f9180b34dc69bd6acde114"
#define GRAPHENE_GIT_REVISION_UNIX_TIMESTAMP 1563572363
#define GRAPHENE_GIT_REVISION_DESCRIPTION "3.2.1"

namespace graphene { namespace utilities {

const char* const git_revision_sha = GRAPHENE_GIT_REVISION_SHA;
const uint32_t git_revision_unix_timestamp = GRAPHENE_GIT_REVISION_UNIX_TIMESTAMP;
const char* const git_revision_description = GRAPHENE_GIT_REVISION_DESCRIPTION;

} } // end namespace graphene::utilities

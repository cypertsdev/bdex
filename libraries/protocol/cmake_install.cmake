# Install script for directory: /root/bdex/libraries/protocol

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/root/bdex/libraries/protocol/libgraphene_protocol.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/graphene/protocol" TYPE FILE FILES
    "/root/bdex/libraries/protocol/include/graphene/protocol/special_authority.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/proposal.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/exceptions.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/object_id.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/vote.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/address.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/operations.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/fba.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/pts_address.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/base.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/config.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/asset_ops.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/witness.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/confidential.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/balance.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/committee_member.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/withdraw_permission.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/vesting.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/assert.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/ext.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/custom.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/market.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/types.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/chain_parameters.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/account.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/worker.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/asset.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/fee_schedule.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/block.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/transaction.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/authority.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/htlc.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/transfer.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/buyback.hpp"
    "/root/bdex/libraries/protocol/include/graphene/protocol/memo.hpp"
    )
endif()


Windows - Visual Studio 2013
============================
#### Prerequisites ####
* Microsoft Visual C++ 2013 Update 1 (the free Express edition will work)
* If you have multiple MSVS installation use MSVS Developer console from target version.
* This build is for 64bit binaries.

#### Set up the directory structure ####
* Create a base directory for all projects.  I'm putting everything in
  `D:\bdtdex`, you can use whatever you like.  In several of the batch files
  and makefiles, this directory will be referred to as `GRA_ROOT`:

```
mkdir D:\bdtdex
```

* Clone the BdtDex Core repository

```
D:
cd D:\bdtdex
git clone https://github.com/bdtdex/bdtdex-core.git
cd bdtdex-core
git submodule update --init --recursive
```

* Download CMake

  Download the latest *Win32 Zip* build CMake from
  http://cmake.org/cmake/resources/software.html (version 2.8.12.2 as of this
  writing).  Unzip it to your base directory, which will create a directory that
  looks something like `D:\bdtdex\cmake-2.8.12.2-win32-x86`.  Rename this
  directory to `D:\bdtdex\CMake`.

  If you already have CMake installed elsewhere on your system you can use it,
  but BdtDex Core has a few batch files that expect it to be in the base
  directory's `CMake` subdirectory, so those scripts would need tweaking.

* Boost

   BdtDex Core depends on the Boost libraries version 1.58 ~ 1.69.  You can build them from
   source.
   * download boost source from http://www.boost.org/users/download/
   * unzip it to the base directory `D:\bdtdex`.
   * This will create a directory like `D:\bdtdex\boost_1_69_0`.

* OpenSSL

   BdtDex Core depends on OpenSSL version 1.0.x or 1.1.x, and you must build this from source.
    * download OpenSSL source from http://www.openssl.org/source/
    * Untar it to the base directory `D:\bdtdex`
    * this will create a directory like `D:\bdtdex\openssl-1.1.1c`.

At the end of this, your base directory should look like this (directory names will
be slightly different for the 64bit versions):
```
D:\bdtdex
+- bdtdex-core
+- boost_1_69_0
+- CMake
+- openssl-1.1.1c
```

#### Build the library dependencies ####

* Set up environment for building:

```
D:
cd D:\bdtdex
notepad setenv_x64.bat
```

Put this into the notepad window, then save and quit.

```
@echo off
set GRA_ROOT=d:\bdtdex
set OPENSSL_ROOT=%GRA_ROOT%\openssl-1.1.1c
set OPENSSL_ROOT_DIR=%OPENSSL_ROOT%
set OPENSSL_INCLUDE_DIR=%OPENSSL_ROOT%\include
set BOOST_ROOT=%GRA_ROOT%\boost_1_69_0

set PATH=%GRA_ROOT%\CMake\bin;%BOOST_ROOT%\lib;%PATH%

echo Setting up VS2013 environment...
call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x86_amd64
```

Then run

```
setenv_x64.bat
```


* Build OpenSSL DLLs
```
D:
cd D:\bdtdex\openssl-1.1.1c
perl Configure VC-WIN64A --prefix=D:\bdtdex\OpenSSL
ms\do_win64a
nmake -f ms\ntdll.mak
nmake -f ms\ntdll.mak install
```

  This will create the directory `D:\bdtdex\OpenSSL` with the libraries, DLLs,
  and header files.

* Build Boost
```
D:
cd D:\bdtdex\boost_1_69_0
bootstrap
.\b2.exe address-model=64
```

#### Build project files for BdtDex Core ####

* Run CMake:

```
D:
cd D:\bdtdex\bdtdex-core
notepad run_cmake_x64.bat
```
Put this into the notepad window, then save and quit.
```
setlocal
call "d:\bdtdex\setenv_x64.bat"
cd %GRA_ROOT%
cmake-gui -G "Visual Studio 12"
```
Then run
```
run_cmake_x64.bat
```

 This pops up the cmake gui, but if you've used CMake before it will probably be
 showing the wrong data, so fix that:
 * Where is the source code: `D:\bdtdex\bdtdex-core`
 * Where to build the binaries: `D:\bdtdex\x64` 

 Then hit **Configure**.  It may ask you to specify a generator for this
 project; if it does, choose **Visual Studio 12 2013 Win64** for 64 bit builds and select **Use default
 native compilers**.  Look through the output and fix any errors.  Then
 hit **Generate**.


* Launch *Visual Studio* and load `D:\bdtdex\x64\BdtDex.sln` 
* Set Active Configuration to `RelWithDebInfo`, ensure Active Solution platform is `x64` for 64 bit builds

* *Build Solution*

Or you can build the `INSTALL` target in Visual Studio which will
copy all of the necessary files into your `D:\bdtdex\install`
directory, then copy all of those files to the `bin` directory.
